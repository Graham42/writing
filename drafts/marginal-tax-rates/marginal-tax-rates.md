# Help me understand tax rates

If I get paid more money how much tax do I pay? Should I be worried about being
in a higher tax bracket? What is a tax bracket? Am I in a competition where the
winner get who pays the most tax gets a reward?

To answer some of these questions we'll explore a system often used for income
tax. We'll being using numbers from the Canadian federal system for the 2018 tax
year in our examples. While the exact number may be out of date, the concept
remains the same.

When you receive a paycheck you can see on the pay stub there are several
deductions that come off the top first before the money lands in your bank
account. Some money goes to the federal government for taxes, more goes to the
provincial government for taxes, and there may be several other deductions such
as Employment Insurance, and the Canada Pension Plan. For the purposes of this
article we will simplify and only look at the federal tax deduction.

Let's turn that into a visual.

Imagine each paycheck arrives as a dump truck full of coins

<img src="./dump-truck-01.svg" width=200
alt="Picture a dump truck filled with coins backing up to a hole in the ground."
/>

The paycheck then gets dumped into a bucket divided into 2 parts. One part goes
to the government as tax, the rest goes to you. In Canada, that's 15% of your
paycheck that is paid to the goverment as federal income tax.

<img src="./dump-truck-02.svg" width=600
alt="The dump truck tilts the bucket and the coins flow down a pipe into a
bucket. The bucket is has a divider so that 15% of the coins are separate
from the rest. This separate part has a label 'Tax: 15%'."
/>

Your paychecks continue to roll in and a portion continues to be set aside for
taxes. However this bucket has a limit, and once it is filled it will be swapped
out for a new bucket.

<img src="./dump-truck-03.svg" width=700
alt="Picture the first bucket is now full. It is pushed aside to be replace
with a new bucket. The bucket is has a divider so that 20.5% of the coins are
separate from the rest. This separate part has a label 'Tax: 20.5%'."
/>

This new bucket has a larger portion for taxes. Now, 20.5% of your paychecks
will be paid to the government as income tax. Take note that this new tax rate
doesn't affect the money you've already earned in the previous bucket.

The paychecks continue to roll in but this bucket also has a limit so it will
reach a point where it too is full and will need to be swapped out for another
bucket.

<img src="./dump-truck-04.svg" width=700
alt="Picture the 2nd bucket is now full. It is pushed aside to be replace
with a 3rd bucket. This bucket is has a divider so that 26% of the coins are
separate from the rest. This separate part has a label 'Tax: 26%'."
/>

Again, this new bucket has a larger portion for taxes. Now, 26% of your
paychecks will be paid to the government as income tax.

At this point we can answer the question: **what is a tax bracket?** These
buckets are tax brackets. At certain thresholds, that is, when a bucket is full,
you get promoted to a new tax bracket. And in each new tax bracket you have a
higher tax rate.

<img src="./buckets.svg" width=200
alt="Picture of different sized buckets"
/>

At the beginning of every year, the counting resets and the taxation begins
again.

But wait. If the counter resets every year why don't you get more money from
your paycheck in January than in December?

This is because your employer follow guidelines for tax deductions that are
based on the **average** amount of tax you will pay over the whole year. Based
on your salary or on your hourly rate you can calculate how much you make on
average each paycheck. Multiply that out and you can aproximate how much you'll
earn in a year. For an example, let us assume that you'll be making $100,000 in
a year which puts us in use of 3 different tax buckets for Canadian federal
taxes.

<img src=""
alt="Picture the 3 buckets from before. The 15% and 20.5% buckets are full
while the 26% bucket is partially filled."
/>

On this $100,000 the first portion up to $46,605 will be taxed at 15% which
amounts to $6,991.

<img src=""
alt="Picture the 15% bucket separated with space between the two parts. The taxed
portion has the actual amount labeled"
/>

The next portion up to $93,208 will be taxed at 20.5%. This bucket started at
$46,605 so once filled it holds $46,603 (93,208 - 46,605 = 46,603). Taxed at
20.5% that amounts to $9,553.

<img src=""
alt="Picture the 20.5% bucket separated with space between the two parts. The taxed
portion has the actual amount labeled. To the left sits the taxed portion of the
15% bucket also labelled with the exact amount."
/>

The final portion up to $100,000 will be tax at 26%. Since this bucket started
at $93,208 it will end up holding $6,792 (100,000 - 93,208 = 6,792). Taxed at
26% that amounts to $1,766.

<img src=""
alt="Picture the 26% bucket separated with space between the two parts. The taxed
portion has the actual amount labeled. To the left sits the taxed portion of the
15% and 20.5% buckets also labelled with the exact amounts."
/>

If we add up the total amount of tax paid: $6,991 + $9,553 + $1,766 = $18,310.
Now we can calculate the **average tax rate** for federal income tax paid for
the whole year: 18,310 / 100,000 = 18.3%. Take note that this is different that
the tax rate of the current tax bucket being filled! The tax rate for the
current bucket is 26%. So if you were to earn $1 more, you would pay $0.26 of
that in tax. This is called your **marginal tax rate** because at the edge of
your current earnings, if you earn $1 more, that is how much tax you would pay.

## Summary

To recap,

A **tax bracket** is like a bucket with a divider. Once you earn more than the
threshold of the bucket, a new bucket starts getting used. The new bucket has a
larger portion that is dedicated to taxes. This repeats for several buckets.

Your **average tax rate** is the amount of tax you pay over the whole year
divided by your total income before taxes.

Your **marginal tax rate** is the percentage you would pay to the government if
you were to earn one additional dollar.

# Reference tables

As per Ontario - 2018 Schedule 1

| Bucket Limit | Marginal Rate |
| ------------ | ------------- |
| 46,605       | 15.0%         |
| 93,208       | 20.5%         |
| 144,489      | 26.0%         |
| 205,842      | 29.0%         |
| No Limit     | 33.0%         |

# Sources

- [Ontario - 2018 Income Tax Package](https://www.canada.ca/en/revenue-agency/services/forms-publications/tax-packages-years/archived-general-income-tax-benefit-package-2018/ontario.html)
- [Ontario - 2018 Schedule 1](https://www.canada.ca/en/revenue-agency/services/forms-publications/tax-packages-years/archived-general-income-tax-benefit-package-2018/ontario.html)

# Tags

Personal Finance, Taxes
