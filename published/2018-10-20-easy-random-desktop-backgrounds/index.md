---
title: Easy Beautiful Desktop Backgrounds
date: 2018-10-20T20:37:24-05:00
tags: desktop automation
spoiler: Many thanks to Unsplash.
image: ./image-918.jpeg
---

Setting up a new computer? Or tired of your old desktop background and just want
something fresh?

Last time I was setting up a computer I needed a quick desktop background, so I
tossed together a script to solve this problem.

![](./image-918.jpeg)

There's an amazing stock photo site called [Unsplash](https://unsplash.com/).
It's tag line is

> Beautiful, free photos. Gifted by the world's most generous community of
> photographers

Leveraging the awesome community on Unsplash,
[David Marby](https://twitter.com/DMarby) put together
[Lorem Picsum](https://picsum.photos/), an API for easy access to placeholder
images.

From there I created a script to pull down and set the desktop background.
Here's the variations depending on your operating system.

- [Linux](#linux)
- [Mac OS X](#mac-os-x)
- Windows (Not available yet)

## Wallpaper Scripts

### Linux

```bash
#!/usr/bin/env bash

# Download random background / lockscreen wallpapers
# For GNOME desktop. Tested on Ubuntu 18.04

set -exo pipefail

WALLPAPER_FOLDER="$HOME/Pictures/Wallpapers"
mkdir -p "$WALLPAPER_FOLDER"

# Change these to match your screen resolution
WIDTH=2560
HEIGHT=1440

downloadRandomImage() {
    # Download a random image for this resolution
    FILENAME=$(cd "$WALLPAPER_FOLDER" &&
        curl --remote-header-name --location --remote-name \
            --silent --write-out "%{filename_effective}" \
            "https://picsum.photos/$WIDTH/$HEIGHT?grayscale")
    # Return the path of the downloaded file
    echo "$WALLPAPER_FOLDER/$FILENAME"
}

DESKTOP_IMAGE=$(downloadRandomImage)
gsettings set org.gnome.desktop.background picture-uri "file://$DESKTOP_IMAGE"

LOCKSCREEN_IMAGE=$(downloadRandomImage)
gsettings set org.gnome.desktop.screensaver picture-uri "file://$LOCKSCREEN_IMAGE"
```

[How to use this script](#how-to-use-these-scripts)

### Mac OS X

```bash
#!/usr/bin/env bash

# Download random background / lockscreen wallpapers
# For Mac OS X

set -exo pipefail

WALLPAPER_FOLDER="$HOME/Pictures/Wallpapers"
mkdir -p "$WALLPAPER_FOLDER"

# Change these to match your screen resolution
WIDTH=2560
HEIGHT=1440

downloadRandomImage() {
    # Download a random image for this resolution
    FILENAME=$(cd "$WALLPAPER_FOLDER" &&
        curl -sJLO -w "%{filename_effective}" \
            "https://picsum.photos/$WIDTH/$HEIGHT?grayscale")
    # Return the path of the downloaded file
    echo "$WALLPAPER_FOLDER/$FILENAME"
}

DESKTOP_IMAGE=$(downloadRandomImage)
osascript -e "tell application 'Finder' to set desktop picture to POSIX file '$DESKTOP_IMAGE'"
```

[How to use this script](#how-to-use-these-scripts)

### Windows

I don't have a Powershell script for this for Windows yet. I may tackle this in
the future.

## How To Use These Scripts

1. Download and install [Visual Studio Code](https://code.visualstudio.com/).
2. Create a new file in VS Code (go to **File > New File**)
3. Copy and paste the script for your operating system into the new file.
4. Update the `WIDTH` and `HEIGHT` values to match the resolution of your
   screen.
5. Save this file in your **Documents** folder as
   **random-unsplash-wallpaper.sh**
6. Open the Terminal. On a Mac you can find this with finder (<kbd>Command +
   Space</kbd>)
7. Now run the following command
   ```bash
   bash ~/Documents/random-unsplash-wallpaper.sh
   ```

Any time you would like to get a new random background, open the terminal and
run the script again.

Enjoy your new backgrounds!

## Bonus round

How would you like a new random wallpaper every day?

We can do this on Linux or Mac with a tool called _crontab_ for running
scheduled tasks.

1. If you're on a Mac follow these instructions for
   [Launching VS Code from the Command Line](https://code.visualstudio.com/docs/setup/mac#_launching-from-the-command-line)
2. Open the Terminal and run the following command to start editing the crontab
   config.
   ```bash
   EDITOR=code crontab -e
   ```
3. Add a new line to this file with
   ```cron
   0 9 * * *   bash ~/Documents/random-unsplash-wallpaper.sh
   ```
4. Save and close the file.

This will configure crontab to run the wallpaper script every day at 9:00 AM if
your computer is on. If you would like to customize this schedule you can build
the appropriate format at
[https://crontab.guru](https://crontab.guru/#0_9_*_*_*)

Thanks for reading!
