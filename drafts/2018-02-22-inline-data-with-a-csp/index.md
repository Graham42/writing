---
title: Inline Data With a Content Security Policy
subtitle: XSS Security, even for “old” apps.
date: 2018-02-22T12:10:02-05:00
---

If I told you about an easy way to make your site nearly invincible to
cross-site scripting (XSS) attacks, would you use it?

<figure style="margin: 0;">
 <img src="./lock-with-keys.jpg" alt="">
 <figcaption style="font-size: 0.8em; text-align: right;">
Photo by <a
href="https://unsplash.com/photos/OnkbHtk_S58?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Ej
Agumbay</a> on <a
href="https://unsplash.com/collections/945911/stock-photo?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a>
 </figcaption>
</figure>

## Beefing up Security

Well, a [Content Security Policy][] (CSP) can do that! It's a simple as adding
an extra header when you serve an html file from the server.

[content security policy]: https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP

I'll link to more information about how you can set this up for your site at the
bottom of this post. For now, here's an example of a strong CSP.

```
Content-Security-Policy: default-src 'self'; form-action 'self';
```

Based on this example header browsers prevent any inline scripts or styles from
running and will only allow resources such as scripts, styles, fonts, etc. to
load from our own domain.

And therein lies the catch: _no inline JavaScript_

## Use case

Allow me to set the stage. We're building a web app, and we need some data from
the server. We're migrating an existing service to the wonders of the New World.
Progressive Web App and all that jazz. However this data we need is terribly
inconvenient to get, because it's not available through any web API.

Our server uses templates to show dynamic content to the user, such as their
name, their age, and what their dog had for breakfast! 🐶

In order for a page to use data from the server in a client side script, you may
have seen a pattern like this before:

```django
<!-- user-template.html - On the server -->
<script type="text/javascript">
  var firstName = "{{ user.firstName }}";
  var age = {{ user.age }};
  // the 'dump' filter calls JSON.stringify on the object
  var pet = {{ user.pet | dump }};

  document.addEventListener("DOMContentLoaded", function() {
    //do work with the variables once the page is ready...
  });
</script>
```

But now, if we serve the above code in our web application with a CSP header
like above, we'll see an error message in our browser console that this script
was not executed.

![Error message: Refused to execute inline script because it violates the following Content Security Policy directive: "default-src 'self'"...](./csp-error.jpg)

Example of a script blocked by a CSP

> Note: Most times this data can (and probably should) come from API calls,
> however there are cases we will run into where that's not an option.

## A Solution: Inline JSON

Luckily there's another easy, and secure, solution.

Instead of inline JavaScript, we can create a `<script>` tag that has type
`application/json`. The browser will not evaluate this as JavaScript so it will
pass the CSP.

```django
<!-- user-template.html -->
<script type="application/json" data-my-app-selector="user-data">
{
  "firstName": "{{ user.firstName }}",
  "age": {{ user.age }},
  "pet": {{ user.pet | dump }}
}
</script>
<script src="main.js"></script>
```

Then in our main JavaScript file we'll parse the JSON and take the value to use
in our app.

```js
// main.js
function getData(dataSelect) {
  try {
    const inlineJsonElement = document.querySelector(
      `script[type="application/json"][data-my-app-selector="${dataSelect}"]`,
    );
    const data = JSON.parse(inlineJsonElement.textContent);
    return data;
  } catch (err) {
    console.error(`Couldn't read JSON data from ${dataSelect}`, err);
  }
}

document.addEventListener("DOMContentLoaded", function() {
  // do work with the variables once the page is ready
  const user = getData("user-data");
  console.log(user.name);
  // ...
});
```

This pattern allows us to have a secure CSP, and still inline data from the
server! Mission accomplished!

All code in this post is available in [a GitHub repo][example repo]. You can
check it out and play with the various secure / insecure options.

[example repo]: https://github.com/Graham42/sample-csp-with-inline-data

---

## Some Alternatives, And Their Downsides

### Data attributes

I've seen some projects use html tags with `data-xyz` attributes to store
values. The downside of this approach is that we would lose data type
information. For example, should the value be a number `4` or a string `"4"`?
For this reason I feel JSON is a better approach.

### Nonce-based CSP

"Nonce" is a fancy name for a "number used once". It should be random and
unguessable. In the context of webpages, we must generate a new nonce _for every
request_, otherwise it's trivial to bypass.

CSPs do allow listing a nonce that script tags can specify, and if the script
tag's nonce matches the nonce in the CSP, it's allowed to run.

The key to effectively using nonces is their unguessable nature. Even if an
attacker manages to inject a script, they have no idea what the nonce in the CSP
header is. The browser will refuse to run their script because the nonce
attribute won't match the one provided in the header.

This brings us to the major problem with this method. It is widely misunderstood
and very easy to do wrong. I have seen numerous threads suggesting the use of
nonces generated at compile/build/bundle time. This is _worse_ than having no
CSP at all because people _think_ they are getting security, but actually _are_
vulnerable. If an attacker knows what the nonce will be, they can provide it as
a valid attribute to an injected script, and the browser will happily run it.

Another drawback is that nonces require an app server. While the above JSON
examples all used a server, there are use cases where we have a statically
generated site and we want to inline some data, for example, a webpack manifest.
For static sites, an app server is unnecessary overhead.

A better alternative to using nonces is to use SHA hashes.

### SHA hash-based CSP

Another option with a CSP is to provide a list of SHA hashes that matches the
content of the inline script blocks that we expect to be in the page. The
browser with verify the content of the script against the hashes provided.

This is actually a pretty good option. The only downside I see is that we'd have
to go through the hassle of setting up tooling to generate hashes for inline
script blocks, and feeding those to our server. It's easier to eliminate all
inline JavaScript and use JSON for data.

---

If you want learn more about Content Security Policies (CSPs) I recommend
checking out the [MDN docs][].

[mdn docs]: https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP

You can build your own CSP using [this interactive tool][] from the folks at
[Report URI][]

[report uri]: https://report-uri.com/
[this interactive tool]: https://report-uri.com/home/generate

Example repo for this post:
https://github.com/Graham42/sample-csp-with-inline-data

---

Thanks for reading! If you want to see more posts from me in the future, follow
me on Twitter [@graham42x](https://twitter.com/Graham42x).
